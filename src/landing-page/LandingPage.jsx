import { useState } from 'react';
import Hero from './Components/Hero.jsx';
import Main from './Components/Main-Komponent.jsx';

function LandingPage() {
  const [groupName] = useState('Propsen');
  return (
    <>
      <Hero groupName={groupName}></Hero>
      <Main></Main>
    </>
  );
}

export default LandingPage;
