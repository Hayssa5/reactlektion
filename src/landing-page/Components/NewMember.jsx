import React, { useState } from 'react';
import Default from './../../assets/Default.jpg';

const NewMember = ({ onAddMember }) => {
  const [newName, setNewName] = useState('');
  const [newCity, setNewCity] = useState('');
  const [showImage, setShowImage] = useState(false);
  

  const handleSubmit = () => {
    // Skapa ett nytt medlemsobjekt med de nya värdena
    const newMember = { image: Default, name: newName, city: newCity };

    onAddMember(newMember);
    setNewName('');
    setNewCity('');
    setShowImage(false);
  };

  return (
    <div className="new-member">
      <input
        type="text"
        placeholder="Namn"
        value={newName}
        onChange={(e) => setNewName(e.target.value)}
      />
      
      {/* Default bilden visas endast efter att man lagt till en ny member */}
      {showImage && (
        <img
          src={Default}
          alt={`Image of new member`}
        />
      )}
      
      <input
        type="text"
        placeholder="Stad"
        value={newCity}
        onChange={(e) => setNewCity(e.target.value)}
      />
      <button onClick={handleSubmit} className="button">
        Lägg till ny medlem
      </button>
    </div>
  );
};

export default NewMember;