import React, {useState} from 'react';


// Tillståndsvariabler för medlemsinformation och redigeringsläge
const MemberCard = ({ image, name: initialName, city: initialCity }) => {
  const [name, setName] = useState(initialName);
  const [city, setCity] = useState(initialCity);
  const [isEditing, setIsEditing] = useState(false);


   // Funktion för att hantera sparande av ändringar
  const handleSave = () => {

    // Uppdatera medlemsobjektet med den nya inputen
    console.log('Uppdatera medlemsobjektet med de nya värdena:', { name, city });

      // Avslutar redigeringsläget (spara)
      setIsEditing(false);
    };

    
  return (
    <div className="member-card">
      <img
        src={image}
        alt={`Image of ${name}`}
      />
{isEditing ? (
  <>
  <div>
    <input type="text"
    value = {name}
    onChange = {(e) => setName(e.target.value)}
  />
  <input type="text"
    value = {city}
    onChange = {(e) => setCity(e.target.value)}
    />
    </div>
  <div>
    <button onClick ={handleSave} className ="button">Spara</button>
  </div>
  </>
): (
  <>
 <div>
  <h3>{name}</h3>
      <p>{city}</p>
    </div>
    <div>
   <button onClick ={() => setIsEditing(true)} className = "button"> Redigera</button>
    </div>
</>
)}
</div>
  
  );
};

export default MemberCard;
