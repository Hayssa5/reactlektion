import React from 'react'
import { Link } from 'react-router-dom'

function NotFoundPage() {
  return (
    <div><h1>Sidan är inte hittad</h1>
    <Link to="/" style={{color: "red", fontSize: "20px"}}>GÅ tillbaka</Link></div>
  )
}

export default NotFoundPage