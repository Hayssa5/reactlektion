import React, { useState, useEffect } from 'react';
import axios from 'axios';
import MemberCard from './MemberCard';
import NewMember from './NewMember';


const Main = () => {
 



  const [members, setMembers] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [initialMembers, setInitialMembers] = useState([]);

  useEffect(() => {
    const getmembers = async () => {

    
    await axios.get("http://localhost:3000/initialMembers") // axios get (från initialMemmbers) hämtar från json filen
      .then(response => {
        setMembers(response.data);
        setInitialMembers(response.data.initialMembers);
      })
      .catch(error => { //om något blir fel, skicka error.
        console.error('Error fetching data:', error);
      });}
      getmembers()
  }, []);

  const handleSearch = (event) => {
    const query = event.target.value;
    setSearchQuery(query);

    const filteredMembers = initialMembers.filter(
      (member) =>
        member.name.toLowerCase().includes(query.toLowerCase()) ||
        member.city.toLowerCase().includes(query.toLowerCase())
    );

    setMembers(filteredMembers);
  };

  return (
    <>
      <div className="main">
        <p>Lite om oss under uppdatering....</p>
        <input
          type="text"
          placeholder="Sök efter namn eller stad"
          value={searchQuery}
          onChange={handleSearch}
        />
      </div>
      {members.map((member, index) => (
        <MemberCard key={index} {...member}></MemberCard>
      ))}
      {/* Här inkluderar du NewMember-komponenten */}
      <NewMember onAddMember={(newMember) => setMembers([...members, newMember])} />
    </>
  );
};

export default Main;
