import React, { useState } from 'react';

const Hero = ({ groupName }) => {
  const [colorToggle, setColorToggle] = useState(false);
  const handleClick = () => {
    setColorToggle((prevColorToggle) => !prevColorToggle);
  };
  console.log();
  const navLinks = [
    { text: 'Home', address: 'home' },
    { text: 'About', address: 'https://www.youtube.com/watch?v=ApOmHYds_TY' },
    { text: 'Contact', address: 'contact' },
  ];

  return (
    <div className="hero">
      <h1
        onClick={handleClick}
        style={{ color: colorToggle ? 'red' : 'blue', cursor: 'pointer' }}
      >
        Välkommen till {groupName}
      </h1>

      <nav className="nyNav">
        <ul className="myList">
          {navLinks.map((link, index) => (
            <li className="myListItem" key={index}>
              <a
                href={link.address}
                target="_blank"
                rel="noopener noreferrer"
              >
                {link.text}
              </a>
            </li>
          ))}
        </ul>
      </nav>
    </div>
  );
};

export default Hero;
