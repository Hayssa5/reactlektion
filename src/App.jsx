import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import LandingPage from './landing-page/LandingPage.jsx';
import NotFoundPage from './landing-page/Components/NotFoundPage.jsx';
import './App.css';
 
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          exact
          path="/"
          element={<LandingPage/>}
        ></Route>
        <Route
          exact
          path="*"
          element={<NotFoundPage/>}
        ></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
